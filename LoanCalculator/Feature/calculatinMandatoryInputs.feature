Feature: Loan calculation using mandatory inputs 

Background: User uses 'Chrom' browser.

Scenario Outline: Calculation for various loan amount
	Given User is on loan calculator page
	And All values are default
	And User enters loan amount <amount>  
	And User enters anual rate 5% 
	And User enters term of 12 months 
	And User clicks Calculate button
	Then Calculation Results are displayed 
	And Correct Loan payments is displayed
	And Correct 12 monthly payments is displayed
	#Then Close browser
Examples: 
    | amount  	|
    | 1000 		|
    | 1000000	|
    | 1000000000|    

                
Scenario Outline: Calculation for various rates
	Given User is on loan calculator page
	And All values are default
	And User enters loan amount 1000  
	And User enters anual rate <rate> 
	And User enters term of 12 months 
	And User clicks Calculate button
	Then Calculation Results are displayed 
	And Correct Loan payments is displayed
	And Correct 12 monthly payments is displayed
	#Then Close browser
Examples: 
    | rate  	|
    | 1% 		|
    | 5%		|
    | 10%		|    

Scenario Outline: Calculation for various number of months
	Given User is on loan calculator page
	And All values are default
	And User enters loan amount 1000  
	And User enters anual rate 5% 
	And User enters term of <months> months 
	And User clicks Calculate button
	Then Calculation Results are displayed 
	And Correct Loan payments is displayed
	And Correct 12 monthly payments is displayed
	#Then Close browser
Examples: 
    | months  	|
    | 5 		|
    | 10		|
    | 24		|    
                