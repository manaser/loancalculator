Feature: Calculate Loan payment with extra inputs 

Background: User uses 'Chrom' browser.


Scenario: Successful Calculation of loan when extra inputs like initial deposit, extra fees and balloon payment are provided.
	Info for tester: LoanCalculatorSimulator does not support calculation with extra inputs. 
	 
	Given User is on loan calculator page
	And All values are default
	And User enters loan amount 1000  
	And User enters anual rate 5% 
	And User enters term of 12 months 
	And User enters initial deposit 200
	And User enters extra fees 100
	And User enters balloon payment at end 600
	#And User enters loan start date 1/1/2016
	And User clicks Calculate button
	Then Loan payments $235.46 is displayed	