Feature: Interface

Background: User uses 'Chrom' browser.


Scenario Outline: Various currencies
	Given User is on loan calculator page
	And All values are default
	When User enters currency <currency> 
	And User enters loan amount 2000  
	And User enters anual rate 5% 
	And User enters term of 12 months 
	And User clicks Calculate button
	Then Calculation Results are displayed 
	And Correct Loan payments is displayed
	#Then Close browser
Examples: 
    | currency  |
    | $ 		|
    | € 		|
    | £			|    
    | 			|        
                
Scenario Outline: Various dates
	Given User is on loan calculator page
	And All values are default
	When User enters currency $
	And User enters loan amount 1000  
	And User enters anual rate 5% 
	And User enters term of <months> months
	And User enters from date <date> 
	And User clicks Calculate button
	Then Calculation Results are displayed 
	And Correct Loan payments is displayed
	#And Correct date is displayed
	And Start date: <startDate> End date: <endDate> are displayed
	#Then Close browser
Examples: 
    |	months	| date  	| startDate			|	endDate			|
    |	12		| 1/1/2016	| January 1, 2016	|	January 1, 2017	|
    |	12		| 6/5/2016	| June 5, 2016		|	June 5, 2017	|
    |	26		| 1/17/2016	| January 17, 2016	|	March 17, 2018	|
                