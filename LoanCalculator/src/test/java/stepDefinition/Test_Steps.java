package stepDefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.*;

import static org.junit.Assert.assertEquals;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.collections.iterators.ArrayListIterator;
import org.junit.BeforeClass;
import org.openqa.selenium.*;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import RMsoft.LoanCalculator.LoanCalculatorSimulator;



@SuppressWarnings("unused")
public class Test_Steps {
	static Logger log = Logger.getLogger("stepDefinition");

	
	private static WebDriver driver;
	String baseUrl = "http://www.thecalculatorsite.com/finance/calculators/loancalculator.php";
	
	private static String defaultCurrency="$";
	private static String currency;
	
	LoanCalculatorSimulator lcs;
	
    public static void openBrowser(){
        //driver = new FirefoxDriver();
        String exePath = "exe\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //currency=defaultCurrency;
    } 
	
	@Given("^User is on loan calculator page$")
	public void User_is_on_loan_calculator_page() throws Throwable {
		if(driver==null) {
			openBrowser();
		}
        currency=defaultCurrency;
		
		driver.get(baseUrl);
		driver.manage().window().maximize();
	}

	@Given("^All values are default$")
	public void All_values_are_default() throws Throwable {
		driver.get(baseUrl);
        currency=defaultCurrency;
	}

	@When("^User enters currency (.*)$")
	public void User_enters_currency(String _currency) throws Throwable {
		if (_currency.matches("(\\$|dolars?)")) {
		    new Select(driver.findElement(By.id("currency"))).selectByVisibleText("Dollar ($)");
	        currency="$";
	        //log.info("currency="+currency);
		} else if (_currency.matches("(\\€|euros?)")) {
		    new Select(driver.findElement(By.id("currency"))).selectByVisibleText("Euro (€)");
	        currency="€";
	        //log.info("currency="+currency);
		} else if (_currency.matches("(\\£|pounds?)")) {
			new Select(driver.findElement(By.id("currency"))).selectByVisibleText("Pound (£)");
	        currency="£";
	        //log.info("currency="+currency);
		} else {
			new Select(driver.findElement(By.id("currency"))).selectByVisibleText("Other");
	        currency="";
	        //log.info("currency="+currency);
		}
	}

	@When("^User enters loan amount (\\d+)$")
	public void setLoanAmount(int amount) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		driver.findElement(By.id("amount")).clear();
		driver.findElement(By.id("amount")).sendKeys(Integer.toString(amount));
	}

	@When("^User enters anual rate (\\d+)%$")
	public void setAnualRate(int rate) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		driver.findElement(By.id("percent")).clear();
		driver.findElement(By.id("percent")).sendKeys(Integer.toString(rate));
	    //throw new PendingException();
	}

	@When("^User enters term of (\\d+) months$")
	public void setTerm(int term) throws Throwable {
		driver.findElement(By.id("term")).clear();
		driver.findElement(By.id("term")).sendKeys(Integer.toString(term));
	    //throw new PendingException();
	}
	
	@When("^User enters initial deposit (\\d+)$")
	public void User_enters_initial_deposit(int deposit) throws Throwable {
		driver.findElement(By.id("deposit")).clear();
		driver.findElement(By.id("deposit")).sendKeys(Integer.toString(deposit));
	}

	@When("^User enters extra fees (\\d+)$")
	public void User_enters_extra_fees(int extrafees) throws Throwable {
		driver.findElement(By.id("extrafees")).clear();
		driver.findElement(By.id("extrafees")).sendKeys(Integer.toString(extrafees));
	}

	@When("^User enters balloon payment at end (\\d+)$")
	public void User_enters_balloon_payment_at_end(int balloon) throws Throwable {
		driver.findElement(By.id("balloon")).clear();
		driver.findElement(By.id("balloon")).sendKeys(Integer.toString(balloon));
	}

	@When("^User enters (from|loan start) date (\\d+)/(\\d+)/(\\d+)$") 
	public void setFrom(String ingore, int day, int month, int year) throws Throwable {
		driver.findElement(By.id("datepicker")).clear();		
		driver.findElement(By.id("datepicker")).sendKeys(String.format("%d/%d/%d", day, month, year));
	}

	@When("^User clicks Calculate button$")
	public void User_clicks_Calculate_button() throws Throwable {
		driver.findElement(By.cssSelector("input.calculatorButton")).click();
	}

	@Then("^(Calculation )Results are displayed$")
	public void Calculation_Results_are_displayed(String na) throws Throwable {
	    assertEquals("Calculation Results", driver.findElement(By.cssSelector("#results1 > h2")).getText());
	}

	@Then("^Loan payments (.)([\\d\\.,]+) is displayed$")
	public void checkLoanPayments(char _currency, String paymentString) throws Throwable {
		//paymentString.replace(",", "");
		//Double payment = Double.parseDouble(paymentString);
		assertEquals(_currency + paymentString, driver.findElement(By.cssSelector("span.resultInfo.total")).getText());
	}
	
	@Then("^Correct Loan payments is displayed$")
	public void Correct_Loan_payments_is_displayed() throws Throwable {
		Double amount;
		Double decimalRate;
		Integer months;
		
		reloadCalculatorSimulator();
		
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
		//otherSymbols.setDecimalSeparator(',');
		//otherSymbols.setGroupingSeparator('.'); 
		DecimalFormat myFormatter = new DecimalFormat(currency+"###,###.##", otherSymbols);
		String expected = myFormatter.format(lcs.getTotalPaymant());
		assertEquals(expected, driver.findElement(By.cssSelector("span.resultInfo.total")).getText());
	    //throw new PendingException();
	}

	@Then("^Correct 12 monthly payments is displayed$")
	public void Correct_monthly_payments_is_displayed() throws Throwable {
		reloadCalculatorSimulator();
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
		DecimalFormat myFormatter = new DecimalFormat(currency+"###,###.##", otherSymbols);
		String expected = myFormatter.format(lcs.getMonthlyPayment());
		assertEquals(expected, driver.findElement(By.xpath(".//*[@id='results1']/span[5]")).getText());
		//assertEquals(expected, driver.findElement(By.cssSelector("resultInfo firepath-matching-node")).getText());
	    //throw new PendingException();
	}
	
	@Then("^Close browser$")
	public void Close_browser() throws Throwable {
		driver.quit();
	}
	
	@Then("^Correct date is displayed$")
	public void Correct_date_is_displayed() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		log.info(driver.findElement(By.xpath(".//*[@id='results1']/div[7]")).getText());
		//assertEquals(_currency + paymentString, driver.findElement(By.cssSelector("date")).getText());
	    throw new PendingException();
	}
	
	@Then("^Start date: (.*) End date: (.*) are displayed$")
			public void datesDisplayed(String startDate, String endDate) throws Throwable {
				//log.info(driver.findElement(By.xpath(".//*[@id='results1']/div[7]")).getText());
				String actual = driver.findElement(By.xpath(".//*[@id='results1']/div[7]")).getText();
				String expected = "Start date: " + startDate + "\nEnd date: " + endDate;
				//log.info("actual=" + actual);
				//log.info("expected=" + expected);
				assertEquals(expected, actual);
			}

	/***
	 *  Help methods
	 */
	private void reloadCalculatorSimulator() {
		Double amount;
		Double decimalRate;
		Integer months;
		
		//assertEquals(driver.findElement(By.cssSelector("span.resultInfo.total")).getText(), "$1,027.29");
		//String amountString = driver.findElement(By.id("amount")).getAttribute("value");
		amount = Double.parseDouble(driver.findElement(By.id("amount")).getAttribute("value"));
		decimalRate = Double.parseDouble(driver.findElement(By.id("percent")).getAttribute("value"))/100.0;
		months = Integer.parseInt(driver.findElement(By.id("term")).getAttribute("value"));
		//System.out.println("amountString=" + amountString);
		
		lcs = new LoanCalculatorSimulator(amount, decimalRate, months);

		//String expected = String.format("%,d.2", lcs.getTotalPaymant());
	}
}
