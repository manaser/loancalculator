package RMsoft.LoanCalculator;

public class LoanCalculatorSimulator {
	/***
	 * 	The loan calculator simulator uses the following formula to calculate loan figures:
		Monthly payment = [ r + r / ( (1+r) ^ months -1) ] x principal loan amount
		Where: r = decimal rate / 12.
		Example:
			For repaying a loan of $1000 at 5% interest for 12 months, the equation would be:
			Monthly payment = [ (0.05 / 12) + (0.05 / 12) / ( (1+ (0.05 / 12)) ^ 12 -1) ] x principal loan amount
			Monthly payment = [ 0.0041666667 + 0.0041666667 / (1.0041666667 ^ 12 - 1)] x 1000.
			Monthly payment = [ 0.0041666667 + 0.0041666667 / (1.0511618983 - 1)] x 1000.
			Monthly payment = [ 0.0041666667 + 0.0041666667 / 0.0511618983] x 1000.
			Monthly payment = [ 0.0041666667 + 0.081440816] x 1000.
			Monthly payment = 0.085607483 x 1000.
			Monthly payment = 85.607483.
	 */
	
	private Double amount;
	private Double decimalRate;
	private Integer months;
	
	//private Double monthlyPayment;
	
	/***
	 * 
	 * @param decimalRate - rate per year in decimal 
	 * @param months - term for which the money is loan
	 * @param amount - Amount to loan
	 */
	public LoanCalculatorSimulator(Double amount, Double decimalRate, Integer months ) {
		super();
		this.amount = amount;
		this.decimalRate = decimalRate;
		this.months = months;
		
	}
	
	/***
	 * 
	 * @return Monthly payment
	 */
	public Double getMonthlyPayment() {
		Double r= decimalRate/12.0;
		//System.out.println("r="+r);
		Double monthlyPayment = (r + r/(Math.pow((1.0+r),months)-1))*amount;
		//System.out.println("monthlyPayment=" + monthlyPayment);
		return monthlyPayment;
	}
	
	/***
	 * 
	 * @return Total payment
	 */
	public Double getTotalPaymant() {
		Double totalPayment = getMonthlyPayment()*months;
		//System.out.println("totalPayment=" + totalPayment);
		return totalPayment;
	}
}
